// JavaScript source code
$('#submit').click(() => {
    var info = {};

    info.fname = $('#first-name').val()
    info.lname = $('#last-name').val()
    info.phone = $('#phone-num').val()
    info.weight = $('#weight').val()
    info.date = $('#date').val()

    if (info.fname != '' && info.lname != '' &&
        info.phone != '' && info.weight != '' && info.date != '') {

        var weight = JSON.parse(localStorage.getItem('registered'));
        if (weight == null) {
            var weight = [];
        }

        var file = document.getElementById('file').files[0];
        if (file == null) {
            swal("Error", "Please Enter a Image", "error");
            return;
        }
        var reader = new FileReader();
        reader.readAsDataURL(file);


        reader.onload = (e) => {
            var img = new Image();
            img.src = reader.result;
            info.image = reader.result;
            weight.push(info);
            localStorage.setItem('registered', JSON.stringify(weight))
            $('#first-name').val('');
            $('#last-name').val('');
            $('#phone-num').val('');
            $('#weight').val('');
            $('#date').val('');
            $('#image').html('')
            swal("Success", "Great Job", "success")
        }
    }
    else {
        $('#first-name').val('');
        $('#last-name').val('');
        $('#phone-num').val('');
        $('#weight').val('');
        $('#date').val('');
        $('#image').html('')
        swal("Error", "Please Fill in the Boxes", "error")
    }
});

$('#file').change(() => {
    var file = document.getElementById('file').files[0];
    var fileType = /image./;
    if (file.type.match(fileType)) {
        var reader = new FileReader();
        reader.readAsDataURL(file);


        reader.onload = (e) => {
            $('#image').html('')
            var img = new Image();
            img.src = reader.result;
            img.width = '300'
            img.height = '200'
            $('#image').append(img)
        }
    }
    else {
        $('image').html('File not supported')
    }
});

$('#footer-submit').click(() => {
    var message = {};

    message.name = $('#name').val()
    message.email = $('#email').val()
    message.mess = $('#message').val()

    if (message.name != '' && message.email != '' && message.mess != '') {

        var send = JSON.parse(localStorage.getItem('Send Message'));
        if (send == null) {
            var send = [];
        }

        send.push(message);
        localStorage.setItem('Send Message', JSON.stringify(send))
        swal("Success", "Thank You for Send a Message", "success")
        $('#name').val('')
        $('#email').val('')
        $('#message').val('')
    }
    else {
        $('#name').val('')
        $('#email').val('')
        $('#message').val('')
        swal("Error", "Please Fill in the Boxes", "error")
    }
});

$('#main-submit').click(() => {
    var main = {};
    main.name = $('#your-name').val()
    main.email = $('#your-email').val()
    main.subject = $('#subject').val()
    main.message = $('#your-message').val()

    if (main.name != '' && main.email != '' && main.subject != '' && main.message != '') {
        var newmain = JSON.parse(localStorage.getItem('New Message'));
        if (newmain == null) {
            var newmain = [];
        }
        newmain.push(main);
        localStorage.setItem('New Message', JSON.stringify(newmain))
        swal("Success", "Thank You for Send a Message", "success")
        $('#your-name').val('')
        $('#your-email').val('')
        $('#subject').val('')
        $('#your-message').val('')
    }
    else {
        $('#your-name').val('')
        $('#your-email').val('')
        $('#subject').val('')
        $('#your-message').val('')
        swal("Error", "Please Fill in the Boxes", "error")
    }
});

function showdata() {

    var info = JSON.parse(localStorage.getItem('registered'));
    var table = '';
    if (info != null) {

        for (var i = 0; i < info.length; i++) {
            for (var j = 0; j < info.length; j++) {
                if (i != j) {
                    if (info[i].phone == info[j].phone && info[i].date == info[j].date) {
                        info.splice(j, 1)
                        break;
                    }
                }
            }
        }

        for (var i = 0; i < info.length; i++) {
            table += `<tr>\
                        <td>${info[i].fname}</td>\
                        <td>${info[i].lname}</td>\
                        <td>${info[i].phone}</td>\
                        <td><img src='${info[i].image}' width='50' height='50'></td>\
                        <td><a data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Display Weight</a>\
                        <div class="col">\
                            <div class="collapse multi-collapse" id="multiCollapseExample1">\
                                    Date: ${info[i].date}<br/>\
                                    Weight: ${info[i].weight}lb\
                            </div>\
                        </div></td>\
                        <td><button class="btn btn-danger" onclick="trash(${i})" ><i class="fa fa-trash"></i></button></td>\
                      </tr>`
        }
        $('#table').html(table)
    }
    else {
        $('#table').html('No Date Show')

    }
}

function trash(i) {
    var info = JSON.parse(localStorage.getItem('registered'));
    info.splice(i, 1)
    localStorage.setItem('registered', JSON.stringify(info))
    showdata();
}